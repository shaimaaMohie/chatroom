/**
 * install this tools before you code
 *
 * sudo npm install -g gulp
 * sudo npm install --save-dev gulp
 * sudo npm install --save-dev gulp-watch
 * sudo npm install --save-dev gulp-obfuscate
 * sudo npm install --save-dev gulp-uglify
 * sudo npm install --save-dev gulp-concat
 * sudo npm install --save-dev gulp-minify-css
 * sudo npm install gulp-sass --save-dev
 *
 * then run gulp watch in your command terminal
 */

// require gulp tools
var gulp = require('gulp');
var watch = require('gulp-watch');
// var obfuscate = require('gulp-obfuscate');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var sass = require('gulp-sass');

// create gulp task for js
gulp.task('js', function() {
    return gulp.src('assets/src/js/*.js') // Matches 'js/target/.js' and resolves 'base' to 'dist'
        .pipe(uglify({
            mangle: true
        }))
        .pipe(concat('script.min.js')) // concatunate all js in one file
        .pipe(gulp.dest('assets/dist/js')); // Writes 'dist/.js'
});


// create gulp task for css
gulp.task('css', function() {
    gulp.src('assets/src/scss/*.scss')
        .pipe(sass())
        .pipe(concat('style.min.css')) // concatunate all css in one file
        .pipe(minifyCss({
            compatibility: 'ie8'
        })) // minify css code
        .pipe(gulp.dest('assets/dist/css'));
});




// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch('assets/src/js/*.js', ['js']);
    gulp.watch('assets/src/scss/*.scss', ['css']);
});
