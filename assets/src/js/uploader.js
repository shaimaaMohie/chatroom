//Function for fire uploader when click on icon
(function() {
    var uploader = {
        init: function() {
            this.cashDom();
            this.bindActions();
        },
        cashDom: function() {
            this.$upload_btn = $('.upload');
            this.$uploader = $('.uploader');
        },
        bindActions: function() {
            this.$upload_btn.on('click', this.triggerUploader.bind(this));
        },
        triggerUploader: function() {
            this.$uploader.trigger('click');
        }
    }
    uploader.init();
})();