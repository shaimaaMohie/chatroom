//Function for filter contacts
(function() {
    var contacts = {
        init: function() {
            this.cashDom();
            this.bindActions();
        },
        cashDom: function() {
            this.$filter_btn = $('.filter_contact').find('.btn');
            this.$list = $('.mycontact').find('li');
            this.$filter_input = $('.filter_contact').find('input');
        },
        bindActions: function() {
            this.$filter_btn.on('click', this.filterList.bind(this));
        },
        filterList: function(e) {
            e.preventDefault();
            var name = this.$filter_input.val();
            this.$list.each(function(i) {
                if (! $(this).find('h3').text().search(name) > -1) {
                console.log($(this));
                }
            });
        }
    }
    contacts.init();
})();