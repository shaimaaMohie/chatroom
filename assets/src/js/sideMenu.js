//Function for open sidebar
(function() {
    var sidebar = {
        init: function() {
            this.cashDom();
            this.bindActions();
        },
        cashDom: function() {
            this.$open_btn = $('.btn-sidebar');
            this.$menu = $('.sidebar-content');
            this.$right_side = $('.right-side');
        },
        bindActions: function() {
            this.$open_btn.on('click', this.openMenu.bind(this));
        },
        openMenu: function(e) {
            e.preventDefault();
            this.$menu.toggleClass('open');
            this.$right_side.toggleClass('open_menu');
        }
    }
    sidebar.init();
})();