<!DOCTYPE html>
<html>
<head>
	<title>Chat task</title>
	<!-- load header partial -->
	<?php include 'partials/header.php';?>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="wrapper-content row">
				<div class="left-side">
					<div class="col-md-3">
						<div class="row left-header">
							<span class="red"></span>
							<span class="orange"></span>
							<span class="green"></span>
						</div>
						<div class="row left-body">
							<button class="btn btn-sidebar"><i class="fa fa-bars fa-lg"></i></button>
							<div class="sidebar-content">
								<ul class="nav nav-tabs">
		                            <li class="active">
			                            <a href="#chat" data-toggle="tab">
			                           		<i class="fa fa-comment fa-lg"></i>
		                            	</a>
		                            </li>
		                            <li>
			                            <a href="#contacts"  data-toggle="tab">
			                            	<i class="fa fa-user fa-lg"></i>
			                            </a>
		                            </li>
		                            <li>
			                            <a href="#search" data-toggle="tab">
			                            	<i class="fa fa-search fa-lg"></i>
			                            </a>
		                            </li>
		                        </ul>
		                        <div class="tab-content">
		                        	<div class="tab-pane fade in active" id="chat">
		            					<ul class="list-unstyled">
		            						<?php for($i=1; $i<=6; $i++) {?>
		            						<li>
		            							<div class="media">
													<div class="media-left media-middle">
														<a href="#">
															<img class="media-object img-circle profile_img" src="assets/dist/img/user.png" alt="...">
														</a>
													</div>
													<div class="media-body">
														<h3>linsey cruz</h3>
														<p>Lorem Ipsum is simply dummy text.</p>  
													</div>
													<span class="time">12:37pm</span>
												</div>
		            						</li>
		            						<?php } ?>
		            					</ul>
		                        	</div>
		                        	<div class="tab-pane fade" id="contacts">
		            					<ul class="list-unstyled">
		            						<?php for($i=1; $i<=6; $i++) {?>
		            						<li>
		            							<div class="media">
													<div class="media-left media-middle">
														<a href="#">
															<img class="media-object img-circle profile_img" src="assets/dist/img/user.png" alt="...">
														</a>
													</div>
													<div class="media-body">
														<h3>linsey cruz</h3>
													</div>
												</div>

		            						</li>
		            						<?php } ?>
		            					</ul>
		                        	</div>
		                        	<div class="tab-pane fade" id="search">
	                        				<div class="form-group form-inline text-center filter_contact">
	                        					<input type="text" name="search" class="form-control">
	                        					<button class="btn btn-default"><i class="fa fa-search"></i></button>
	                        				</div>
			            					<ul class="list-unstyled mycontact">
			            					<?php for($i=1; $i<=2; $i++) {?>
		            						<li>
		            							<div class="media">
													<div class="media-left media-middle">
														<a href="#">
															<img class="media-object img-circle profile_img" src="assets/dist/img/user.png" alt="..." >
														</a>
													</div>
													<div class="media-body">
														<h3>linsey cruz</h3>
													</div>
												</div>
		            						</li>
		            						<?php } ?>
		            						<?php for($i=1; $i<=2; $i++) {?>
		            						<li>
		            							<div class="media">
													<div class="media-left media-middle">
														<a href="#">
															<img class="media-object img-circle profile_img" src="assets/dist/img/user.png" alt="..." >
														</a>
													</div>
													<div class="media-body">
														<h3>calvin carter</h3>
													</div>
												</div>
		            						</li>
		            						<?php } ?>
		            					</ul>
		                        	</div>
		                        </div>

							</div>
						</div>
					</div>
				</div>
				<div class="right-side">
					<div class="col-md-9">
						<div class="row right-header">
							<img src="assets/dist/img/user.png" class="img-circle profile_img">
							<span class="contact-name">chat with linsey cruz</span>
							<a href="#"><i class="fa fa-cog fa-lg"></i></a>
						</div>
						<div class="row right-body">
							<div class="chat">
								<?php for($i=1; $i<=3; $i++) {?>
								<div class="chat-wrapper">
										<div class="chat-box right-arrow">
											<div class="text">
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
											</div>
										</div>
										<div class="time">12:00 pm</div>
								</div>
								<?php } ?>
								<?php for($i=1; $i<=3; $i++) {?>
								<div class="chat-wrapper">
									<div class="chat-box left-arrow">
										<div class="text">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
										</div>
									</div>
									<div class="time">12:00 pm</div>
								</div>
								<?php } ?>
							</div>
							<div class="input-box">
								<div class="form-group">
									<a href="#" class="upload"><i class="fa fa-paperclip fa-2x"></i></a>
								    <input type="file" name="" class="form-control uploader">
									<input type="text" name="message" class="form-control" placeholder="type your message">
									<button class="btn btn-send">send</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- load footer partial -->
<?php include 'partials/footer.php';?>

</body>
</html>